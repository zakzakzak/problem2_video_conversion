<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Video converter</title>
  </head>
  <body>

    <div class="ui container">
      <h4>Video converter</h4>

      <form action="/uploadfile/" method="POST" enctype="multipart/form-data">
        <label for="">Upload file</label>
        <input type="file" name="file" value="" accept="video/*">

        <label for="">Convert to :</label>
        <select id="cars" name="conv">
          <option value="mp4">MP4</option>
          <option value="avi">AVI</option>
          <option value="flv">FLV</option>
        </select>
        <button type="submit" name="submit">Upload</button>
      </form>
    </div>

  </body>
</html>
