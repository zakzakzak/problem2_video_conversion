import uvicorn
# import ffmpeg
import os, shutil
from pydantic import BaseModel, Field
from fastapi import Request, Depends, BackgroundTasks, Form, File, UploadFile, FastAPI as fastapi
from typing import List
from starlette.responses import PlainTextResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from starlette.responses import FileResponse
import subprocess
import random
import string

# print("test library ok")
app = fastapi()
templates = Jinja2Templates(directory="templates")


# Method
def get_random_alphanumeric_string(length):
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(length)))
    return result_str
# Method


@app.get("/")
async def home(request : Request):
    return templates.TemplateResponse("home.php",{
        "request" : request
    })

# UPLOAD dan CONVERSION
@app.post("/uploadfile/")
async def create_upload_file(request : Request , file: UploadFile = File(...), conv : str = File(...)):
    upload_folder = "/app/upload"
    file_object = file.file

    file_name_original = file.filename.split(".")


    random_name    = get_random_alphanumeric_string(20)
    file_name_new  = random_name + "." + file_name_original[len(file_name_original) - 1]
    file_name_out  = random_name + "." + conv

    upload_folder = open(os.path.join(upload_folder, file_name_new), 'wb+')
    shutil.copyfileobj(file_object, upload_folder)
    upload_folder.close()

    input  = "/app/upload/" + file_name_new
    output = "/app/conversion/" + file_name_out

    if (conv == "mp4"):
        cmd    = 'ffmpeg -i "'+input+'"  "'+output+'"'
    elif(conv == "avi"):
        cmd    = 'ffmpeg -i "'+input+'"  "'+output+'"'
    elif(conv == "flv"):
        cmd    = 'ffmpeg -i "'+input+'"  "'+output+'"'

    try:
        x    = subprocess.check_output(cmd, shell=True)
        return templates.TemplateResponse("download.php",{
          "request" : request,
          "file_name" : file_name_out,
          "file_name_original" :  file_name_original[0] + "." + conv,
          "opti_conv" : conv
          })
    except:
        return {"Pesan" : "File inputan bermasalah"}




# DOWNLOAD
@app.get("/shows/{file_name}/{file_name_original}/{opti_conv}")
def get_items(request : Request, file_name : str, file_name_original : str, opti_conv : str):
    file_location = "/app/conversion/" + file_name
    # file_name_original = file_name_original.split('.')
    # file_name_original = file_name_original[0] + opti_conv + "." + file_name_original[1]
    return FileResponse(file_location, media_type='application/octet-stream',filename=file_name_original)



if __name__ == "__main__":
    uvicorn.run(app, host = "0.0.0.0", port = 6000)
