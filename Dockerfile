FROM python:3

ADD main.py /
ADD testing.py /

RUN apt-get update && \
    apt-get -y install ffmpeg && \
    pip install --upgrade pip && \
    pip install uvicorn && \
    pip install pydantic && \
    pip install fastapi && \
    pip install starlette && \
    pip install jinja2 && \
    pip install python-multipart && \
    pip install aiofiles && \
    pip install requests

WORKDIR /app

COPY . /app

EXPOSE 6000

CMD [ "python", "./main.py" ]
